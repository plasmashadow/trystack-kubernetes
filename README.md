## Trystack - kubernetes

Setting up kubernetes cluster on trystack.


## Pre requisits (Minimum requirement)

* 3 Nova nodes
* 2 GB ram per node
* minimum 10GB harddisk per node.


## Pre Configuration

All configuration in above demo are set to some standard hostnames

* master
* kcnet-2 (Minion - 1)
* kcnet-3 (Minion - 2)

```
vim assets/hosts

# file assets/hosts

# 192.168.1.3 master
# 192.168.1.4 kcnet-2
# 192.168.1.5 kcnet-3

```

Edit the above file to change the /etc/hosts configuration.

Configure the trystack infrastructure with the help of below link.

https://edwardsamuel.wordpress.com/2014/10/25/tutorial-creating-openstack-instance-in-trystack/


## Running playbook

Run the below command to execute the ansible playbook

```
ansible-playbook playbook.yml

```



Thanks!!!